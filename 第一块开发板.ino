#include "DHT.h"
#include <Arduino.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* wifi_ssid = "1+11";
const char* wifi_password = "000000000";

const char *mqtt_broker_addr = "10.12.112.130"; // 服务器地址
const uint16_t mqtt_broker_port = 1883; // sk服务端口号            
String mqtt_client_id = "esp8266_client_13"; // 客户端ID

WiFiClient tcpClient;
PubSubClient mqttClient;

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0,SCL,SDA);

#define mqAPin A0
#define mqDPin 2
#define fanpin 16
#define button1 12
#define button2 14


int mqBite=0;
int mqVal=0;
float mqVot=0;
int mod_flag;
int fan_flag=1;
int fan_flag2 = 1;
int select_flag = 0;
double t,h;
unsigned long previousConnectMillis = 0; // 毫秒时间记录
const long intervalConnectMillis = 1000; // 时间间隔
unsigned long previousPublishMillis = 0; // 毫秒时间记录
const long intervalPublishMillis = 1000; // 时间间隔
char temp[5],her[5];

void setup(){
  Serial.begin(9600);
  pinMode(mqAPin, INPUT);
  pinMode(button1,INPUT);
  pinMode(button2,INPUT);
  u8g2.begin();
  u8g2.clearBuffer();
  // 连接网络
  Serial.printf("\nConnecting to %s", wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED)
  {
      delay(50);
      Serial.print(".");
  }
  Serial.println("Wifi ok");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // 设置MQTT客户端
  mqttClient.setClient(tcpClient);
  mqttClient.setServer(mqtt_broker_addr, mqtt_broker_port);
  mqttClient.setCallback(mqtt_callback);
}

void mqtt_callback(char *topic, byte *payload, unsigned int length)
{
    Serial.printf("Message arrived in topic %s, length %d\n", topic, length);
    Serial.print("Message:");
    for (int i = 0; i < length; i++)
    {
        Serial.print((char)payload[i]);
    }
    Serial.println("\n----------------END----------------");
    if(topic[0] == 't'){
      for (int i = 0; i < length; i++)
    {
        temp[i]=payload[i];
    }
    t = atof(temp);
    Serial.println(t);
      }
     if(topic[0] == 'h'){
      for (int i = 0; i < length; i++)
    {
        her[i]=payload[i];
    }
    h = atof(her);
    Serial.println(h);
      }
    if(topic[0] == 's' && payload[0] == '1'){
        select_flag++;
        analogWrite(fanpin,0);
    }
    if(topic[0] == 'f' && payload[0] == '1'){
        fan_flag2++;
    }
}

void ShowText(float t,float h,float mqVot,int mod_flag){
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_unifont_t_chinese2); 
  char str[30];
  sprintf(str,"Temp: %.2f",t);
  u8g2.drawUTF8(0,10,str);
  sprintf(str,"Humi: %.2f",h);
  u8g2.drawUTF8(0,27,str);   
  sprintf(str,"CO: %.2f",mqVot);
  u8g2.drawUTF8(0,40,str); 
  if(mod_flag == 1){
  sprintf(str,"Mode: Hand");}
  else sprintf(str,"Mode: Auto");
  u8g2.drawUTF8(0,55,str); 
  u8g2.sendBuffer();  

}

void ShowText2(int fan_flag){
      u8g2.clearBuffer();
      u8g2.setFont(u8g2_font_unifont_t_chinese2); 
      u8g2.drawUTF8(0,10,"Mode: Hand");
      char str[30];
      sprintf(str,"Fan: %d",fan_flag);
      u8g2.drawUTF8(0,30,str);
      u8g2.sendBuffer();

}

void mqttshow(float mqVot){
    if (mqttClient.connected())
    {
        unsigned long currentMillis = millis();
        if (currentMillis - previousPublishMillis >= intervalPublishMillis) // 如果和前次时间大于等于时间间隔
        {
            previousPublishMillis = currentMillis;
            char str[10];
            sprintf(str,"%f",mqVot);
            mqttClient.publish("CO2",str);
        }
    }
    mqttClient.loop();

}

void loop(){
  unsigned long currentMillis = millis(); // 读取当前时间
    // 连接MQTT服务器
    if (!mqttClient.connected()) // 如果未连接
    {
        if (currentMillis - previousConnectMillis > intervalConnectMillis)
        {
            previousConnectMillis = currentMillis;
            if (mqttClient.connect(mqtt_client_id.c_str())) // 尝试连接服务器
            {
              mqttClient.subscribe("t");
              mqttClient.subscribe("h");
              mqttClient.subscribe("select");
              mqttClient.subscribe("fanflag");
            }
        }
    }
  mqttClient.loop();
  A:
  mqVal = analogRead(mqAPin);
  mqVot = mqVal*0.0049;

  mqttshow(mqVot);
  ShowText(t,h,mqVot,mod_flag);

  if(mod_flag==0 && select_flag % 2 ==0)
  {
  if(t>=24 && t<=25){
      analogWrite(fanpin,500); 
      //delay(500);
   }
  else if(t>=25 && t<=26){
        analogWrite(fanpin,1000); 
       // delay(500);
   }
   else if(t>26){
        analogWrite(fanpin,2000); 
        //delay(500);
   }
   else 
   {
        analogWrite(fanpin,0); 
        //delay(500);
      }
 } // auto
  mod_flag=digitalRead(button1);
  Hand:
    if(mod_flag == 1 && select_flag % 2 == 0){
    fan_flag = 1;
    Start:
    while(1){
    mod_flag =0;
    ShowText2(fan_flag);
    if(fan_flag == 1){
      delay(100);
      analogWrite(fanpin,1000);
      ShowText2(fan_flag);
      mqttshow(mqVot);
      while(1){
        fan_flag = 0;
        mod_flag = 0;
        for(int i=0;i<50;i++){
        delay(1);
        fan_flag = digitalRead(button2);
        mod_flag = digitalRead(button1);
        mqttshow(mqVot);
        if(fan_flag == 1){analogWrite(fanpin,0); delay(200);fan_flag=0; goto Start;}
        if(mod_flag == 1){analogWrite(fanpin,0);delay(200);mod_flag = 1; goto A; }}
        }
    }
     for(int i=0;i<50;i++){
     delay(1);
     fan_flag = digitalRead(button2);
     mod_flag = digitalRead(button1);
     if(mod_flag == 1){analogWrite(fanpin,0);delay(200);mod_flag = 1; goto A; }
     }
     
   }
 }
 if(select_flag % 2 == 1){
    fan_flag = 1;
    Start2:
    while(1){
    ShowText2(fan_flag2);
    if(fan_flag2 == 1){
      delay(100);
      analogWrite(fanpin,1000);
      ShowText2(fan_flag2);
      mqttshow(mqVot);
      while(1){
        fan_flag2 = 0;
        for(int i=0;i<50;i++){
        delay(1);
        mqttClient.loop();
        mqttshow(mqVot);
        if(select_flag %2 == 0){goto A;}
        if(fan_flag2 %2 == 1){analogWrite(fanpin,0); delay(200);fan_flag2=0; goto Start2;}
          }
        }
    }
     for(int i=0;i<50;i++){
     delay(1);
     mqttClient.loop();
     if(select_flag %2 == 0){goto A;}

     }
     
   }
 }
}
