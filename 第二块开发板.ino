#include "DHT.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define DHTPIN 13     
#define DHTTYPE DHT11  
#define beeppin 15
DHT dht(DHTPIN, DHTTYPE);

float h;
float t;
int beep_flag;

const char* ssid = "OPPO Find X5 Pro";//替换自己的WIFI
const char* password = "i29mxm5p";//替换自己的WIFI
const char* mqtt_server = "10.12.112.130";//替换自己的MQTT服务器IP地址
WiFiClient espClient;   //Wifi连接对象定义
PubSubClient client(espClient); ;   //MQTT客户端对象定义
long lastMsg = 0;//用来保存上一次接收TOPIC的时间
char msg[500];//用来存放ESP8266将要PUBLISH到服务器的消息



void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);//串口显示提示文字Connecting to ssid
  WiFi.begin(ssid, password);//开始连接ssid网络
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }//如果未连接上WiFi，串口每半0.5秒打印一个.持续等待
  //运行到此处时，wifi已连接
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());//如果连接上WiFi，串口显示WiFi connected以及ESP8266开发板的IP地址
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");//如果ESP8266开发板还没有连上MQTT服务器，串口输出Attempting MQTT connection...
    String clientId = "ESP8266Client-114514";//给ESP8266开发板随机生成一个客户端ID号，在它连上MQTT服务器后，我们在MQTT服务器网页上能够查看到
    // Attempt to connect
   if (client.connect(clientId.c_str())) {
      Serial.println("connected");//如果ESP8266开发板连上了MQTT服务器，串口输出connected
        client.subscribe("CO2");
    } else {
      Serial.print("failed, rc=");//如果ESP8266开发板还是没有连上MQTT服务器，串口输出failed, rc=
      Serial.print(client.state());//串口输出错误代码
      Serial.println(" try again in 5 seconds"); // Wait 5 seconds before retrying
      delay(5000);
    } // 尝试连接的程序块
  } //while循环结束
}//函数结束

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");//串口显示Message arrived [topic]，主题（topic）是led或者inTopic
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }//payload是具体的消息，比如Hello World；length是消息长度；payload是个BYTE数组，本次实验中发送数据是字符串，因此这里将字符串的每个字符依次打印出
  Serial.println();
  if(topic[0]=='C'){
  if (payload[0] >= '5') {
    beep_flag=1;
  }
  else{
    beep_flag=0;
    }
  }
}

void setup(){
  Serial.begin(115200);
  pinMode(beeppin,OUTPUT);
  dht.begin();

  //以下依次调用连接WiFi、设置MQTT服务器、回调（接收并处理来自MQTT服务器的消息）、连接MQTT服务器等函数
  setup_wifi();
  
  randomSeed(micros());//根据当前时间产生随机数种子
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);  //此处注册处理消息的函数callback
  reconnect();
   while(!client.subscribe("CO2"))
  {Serial.println("订阅失败;尝试重新订阅！");
   client.subscribe("CO2");
   delay(300);
  }
  //如果成功订阅主题led，串口输出订阅成功~~~
  Serial.println("订阅成功~~~");
    
}



void loop(){

    if (!client.connected()) 
  {
      reconnect();
     }
     client.loop(); 
   h = dht.readHumidity();             
   t = dht.readTemperature(); 
    if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  /*
    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.println(F("°C "));
*/
    
    snprintf (msg, 5, "%.2f",h);
    client.publish("h",msg);   
    snprintf (msg, 5, "%.2f",t);
    client.publish("t",msg);
     if(beep_flag==1){
      tone(beeppin,800);
      delay (200);  
      noTone(beeppin);
      delay (200);  
  }
    else
    {
      delay(1000);
      }
}
